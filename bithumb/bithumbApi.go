package bithumb

import (
	"bytes"
	"encoding/json"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"
)

type tickerData struct {
	OpeningPrice string `json:"opening_price"` // 최근 24시간 내 시작 거래금액
	ClosePrice   string `json:"close_price"`   // 최근 24시간 내 마지막 거래금액
	MinPrice     string `json:"min_price"`     // 최근 24시간 내 최저 거래금액
	MaxPrice     string `json:"max_price"`     // 최근 24시간 내 최고 거래금액
	AveragePrice string `json:"average_price"` // 최근 24시간 내 평균 거래금액
	UnitTraded   string `json:"units_traded"`  // 최근 24시간 내 Currency 거래량
	Volume1Day   string `json:"volume_1day"`   // 최근 1일간 Currency 거래량
	Volume7Day   string `json:"volume_7day"`   // 최근 7일간 Currency 거래량
	BuyPrice     string `json:"buy_price"`     // 거래 대기건 최고 구매가
	SellPrice    string `json:"sell_price"`    // 거래 대기건 최소 판매가
	Date         string `json:"date"`          // 현재 시간 Timestamp
}
type concurrency struct {
	BTC  tickerData `json:"BTC"`
	ETH  tickerData `json:"ETH"`
	DASH tickerData `json:"DASH"`
	LTC  tickerData `json:"LTC"`
	ETC  tickerData `json:"ETC"`
	XRP  tickerData `json:"XRP"`
	BCH  tickerData `json:"BCH"`
	XMR  tickerData `json:"XMR"`
	ZEC  tickerData `json:"ZEC"`
	QTUM tickerData `json:"QTUM"`
	BTG  tickerData `json:"BTG"`
	EOS  tickerData `json:"EOS"`
}
type TickerResult struct {
	Status string     `json:"status"` // 결과 상태 코드 (정상 : 0000, 정상이외 코드는 에러 코드 참조)
	Data   tickerData `json:"data"`
}
type TickerAll struct {
	Status string      `json:"status"` // 결과 상태 코드 (정상 : 0000, 정상이외 코드는 에러 코드 참조)
	Data   concurrency `json:"data"`
}
type transInfo struct {
	Quantity string `json:"quantity"` // 양
	Price    string `json:"price"`    // 가격
}
type orderData struct {
	OrderCurrency string      `json:"order_currency"`
	Sell          []transInfo `json:"asks"` // 판매 요청
	Buy           []transInfo `json:"bids"` // 구매 요청
}
type orederWrapper struct {
	Time    string    `json:"timestamp"`
	Payment string    `json:"payment_currency"`
	BTC     orderData `json:"BTC"`
	ETH     orderData `json:"ETH"`
	DASH    orderData `json:"DASH"`
	LTC     orderData `json:"LTC"`
	ETC     orderData `json:"ETC"`
	XRP     orderData `json:"XRP"`
	BCH     orderData `json:"BCH"`
	XMR     orderData `json:"XMR"`
	ZEC     orderData `json:"ZEC"`
	QTUM    orderData `json:"QTUM"`
	BTG     orderData `json:"BTG"`
	EOS     orderData `json:"EOS"`
}

type OrderAll struct {
	Status string        `json:"status"`
	Data   orederWrapper `json:"data"`
}

var apiKey = "cb26ba88e44c79602c11a3710c55018f "
var apiSecret = "502bf98e7b3ba5d6c9e5e4fd15315f0b"

func xcoinApiCall(category string) []byte {

	client := &http.Client{}
	httpReq, _ := http.NewRequest("POST", "https://api.bithumb.com/public/"+category+"/ALL", nil) // URL-encoded payload

	//e_endpoint := url.QueryEscape(endpoint);

	httpReq.Header.Add("Api-Key", apiKey)
	httpReq.Header.Add("Api-Sign", apiSecret)
	httpReq.Header.Add("Content-Type", "application/x-www-form-urlencoded")

	resp, err := client.Do(httpReq)
	if err != nil {
		log.Print(err)
	}

	resp_data, err := ioutil.ReadAll(resp.Body)

	return resp_data
}
func xcoinPostApiCall(category string, param string) []byte {

	client := &http.Client{}

	form := url.Values{
		"count": {param},
	}
	body := bytes.NewBufferString(form.Encode())
	httpReq, _ := http.NewRequest("POST", "https://api.bithumb.com/public/"+category+"/ALL", body) // URL-encoded payload

	//e_endpoint := url.QueryEscape(endpoint);

	httpReq.Header.Add("Api-Key", apiKey)
	httpReq.Header.Add("Api-Sign", apiSecret)
	httpReq.Header.Add("Content-Type", "application/x-www-form-urlencoded")

	resp, err := client.Do(httpReq)
	if err != nil {
		log.Print(err)
	}

	resp_data, err := ioutil.ReadAll(resp.Body)

	return resp_data
}

// GetCoinTicker is
func GetCoinTicker() (TickerAll, []byte) {
	var tickerAll = TickerAll{}
	body := xcoinApiCall("ticker")
	err := json.Unmarshal(body, &tickerAll)
	if err != nil {
		log.Print(err)
	}
	return tickerAll, body
}
func OrderJson() []byte {

	body := xcoinPostApiCall("orderbook", "5")
	return body
}
func TickerJson() []byte {

	body := xcoinApiCall("ticker")
	return body
}
func GetCoinOrder(count string) (OrderAll, []byte) {
	var orderAll = OrderAll{}
	body := xcoinPostApiCall("orderbook", "5")
	err := json.Unmarshal(body, &orderAll)
	if err != nil {
		log.Print(err)
	}
	return orderAll, body
}
