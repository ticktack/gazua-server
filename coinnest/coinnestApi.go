package coinnest

import (
	"io/ioutil"
	"log"
	"net/http"
)

//btc,bt1,bt2,bch,btg,bcd,eth,etc,ada,qtum,xlm,neo,gas,rpx,hsr,knc,tsl,tron,omg,wtc,mco,ink,ent
func xcoinApiCall(category string, coin string) []byte {
	resp, err := http.Get("https://api.coinnest.co.kr/api/pub/" + category + "?coin=" + coin)
	if err != nil {
		log.Print(err)
	}

	respData, err := ioutil.ReadAll(resp.Body)

	return respData
}
func GetCoinTicker() []byte {
	body := xcoinApiCall("ticker", "btc")
	return body
}

func GetCoinOrder() []byte {
	body := xcoinApiCall("trades", "btc")
	return body
}
