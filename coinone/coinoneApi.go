package coinone

import (
	"io/ioutil"
	"log"
	"net/http"
)

//btc, bch, eth, etc, xrp, qtum, iota, ltc, btg
func xcoinApiCall(category string, coin string) []byte {
	resp, err := http.Get("https://api.coinone.co.kr/" + category + "?currency=" + coin)
	if err != nil {
		log.Print(err)
	}

	respData, err := ioutil.ReadAll(resp.Body)

	return respData
}
func GetCoinTicker() []byte {
	body := xcoinApiCall("ticker", "all")
	return body
}

func GetCoinOrder() []byte {
	body := xcoinApiCall("orderbook", "btc")
	return body
}
