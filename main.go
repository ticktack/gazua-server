package main

import (
	"gazua-server/bithumb"
	"gazua-server/channel"
	"gazua-server/coinnest"
	"gazua-server/coinone"
	"gazua-server/korbit"
	"html/template"
	"net/http"
	"path/filepath"
	"sync"
	"time"

	"github.com/gorilla/mux"
)

var bithumbChan channel.Bithumb
var bithumbTicker bithumb.TickerAll
var bithumbOrder bithumb.OrderAll

var bithumbTickerJson []byte
var bithumbOrderJson []byte
var coinnestTickerJson []byte
var coinnestOrderJson []byte
var coinoneTickerJson []byte
var coinoneOrderJson []byte
var korbitTickerJson []byte
var korbitOrderJson []byte

func main() {
	go CoinRefreshRoutine()
	r := mux.NewRouter()
	t := TemplateHandler{}
	t.Filename = "test.html"
	r.HandleFunc("/ticker/{coin}", t.tickerInquiryHTTP).Methods("GET")
	r.HandleFunc("/json/ticker/{site}/{coin}", tickerJsonHTTP).Methods("GET")
	r.HandleFunc("/json/order/{site}/{coin}", orderJsonHTTP).Methods("GET")
	// r.HandleFunc("/test", testHTTP).Methods("GET")
	fs := http.FileServer(http.Dir("./dist"))
	r.PathPrefix("/").Handler(fs)

	http.ListenAndServe(":8000", r)
}
func CoinRefreshRoutine() {
	// bithumbChan = channel.Bithumb{}
	// bithumbChan.OpenChannel()

	for {
		bithumbTicker, bithumbTickerJson = bithumb.GetCoinTicker()
		bithumbOrder, bithumbOrderJson = bithumb.GetCoinOrder("5")
		coinnestTickerJson = coinnest.GetCoinTicker()
		coinnestOrderJson = coinnest.GetCoinOrder()
		coinoneTickerJson = coinone.GetCoinTicker()
		coinoneOrderJson = coinone.GetCoinOrder()
		korbitTickerJson = korbit.GetAllCoinTicker()
		korbitOrderJson = korbit.GetAllCoinOrder()
		// if ticker.Status != "0000" {
		// 	time.Sleep(time.Millisecond)
		// 	continue
		// }
		// bithumbChan.TickerChan <- ticker
		// bithumbChan.OrderChan <- order
		time.Sleep(time.Millisecond * 2)
	}
}
func KorbitRefreshRoutine() {
	for {
		time.Sleep(time.Millisecond * 2)
	}
}

type TemplateHandler struct {
	once     sync.Once
	Filename string
	templ    *template.Template
}

func orderJsonHTTP(w http.ResponseWriter, r *http.Request) {
	w.Header().Add("Content-Type", "application/json")
	w.Header().Add("Access-Control-Allow-Origin", "*")
	vars := mux.Vars(r)
	switch vars["site"] {
	case "bithumb":
		w.Write(bithumbOrderJson)
		break
	case "coinnest":
		w.Write(coinnestOrderJson)
		break
	case "coinone":
		w.Write(coinoneOrderJson)
		break
	case "korbit":
		w.Write(korbitOrderJson)
		break
	}
}
func tickerJsonHTTP(w http.ResponseWriter, r *http.Request) {
	w.Header().Add("Content-Type", "application/json")
	w.Header().Add("Access-Control-Allow-Origin", "*")
	vars := mux.Vars(r)
	switch vars["site"] {
	case "bithumb":
		w.Write(bithumbTickerJson)
		break
	case "coinnest":
		w.Write(coinnestTickerJson)
		break
	case "coinone":
		w.Write(coinoneTickerJson)
		break
	case "korbit":
		w.Write(korbitTickerJson)
		break
	}
}

func (t *TemplateHandler) tickerInquiryHTTP(w http.ResponseWriter, r *http.Request) {
	// bithumbTicker := <-bithumbChan.TickerChan
	// bithumbOrder := <-bithumbChan.OrderChan
	t.once.Do(func() {
		t.templ = template.Must(template.ParseFiles(filepath.Join(t.Filename)))
	})
	data := make(map[string]interface{})
	data["ticker"] = bithumbTicker
	data["order"] = bithumbOrder
	t.templ.Execute(w, data)
}
