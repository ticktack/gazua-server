package korbit

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"net/http"
)

var korbitCoinKinds = [5]string{"btc_krw", "eth_krw", "etc_krw", "xrp_krw", "bch_krw"}

type tickerAll struct {
	BTC ticker `json:"BTC"`
	ETH ticker `json:"ETH"`
	ETC ticker `json:"ETC"`
	XRP ticker `json:"XRP"`
	BCH ticker `json:"BCH"`
}
type ticker struct {
	Timestamp string `json:"timstamp"`
	Last      string `json:"last"`
}

//btc, bch, eth, etc, xrp, qtum, iota, ltc, btg
func xcoinApiCall(category string, coin string) []byte {
	resp, err := http.Get("https://api.korbit.co.kr/v1/" + category + "?currency_pair=" + coin)
	if err != nil {
		log.Print(err)
	}

	respData, err := ioutil.ReadAll(resp.Body)

	return respData
}
func GetCoinTicker(coin string) []byte {
	body := xcoinApiCall("ticker", coin)
	return body
}

func GetCoinOrder(coin string) []byte {
	body := xcoinApiCall("orderbook", coin)
	return body
}

func GetAllCoinOrder() []byte {
	result := "{"
	for i, v := range korbitCoinKinds {
		Ticker := ticker{}
		err := json.Unmarshal(GetCoinOrder(v), &Ticker)
		if err != nil {
			log.Print(err)
		}
		result += "\"" + v + "\"" + ":" + string(GetCoinOrder(v))
		if i < len(korbitCoinKinds)-1 {
			result += ","
		}
	}
	result += "}"
	return []byte(result)
}

func GetAllCoinTicker() []byte {
	result := "{"
	for i, v := range korbitCoinKinds {
		result += "\"" + v + "\"" + ":" + string(GetCoinTicker(v))
		if i < len(korbitCoinKinds)-1 {
			result += ","
		}
	}
	result += "}"
	return []byte(result)
}
