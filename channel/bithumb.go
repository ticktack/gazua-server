package channel

import (
	"gazua-server/bithumb"
)

type Bithumb struct {
	TickerChan chan bithumb.TickerAll
	OrderChan  chan bithumb.OrderAll
}

func (bithumbChan *Bithumb) OpenChannel() {
	bithumbChan.TickerChan = make(chan bithumb.TickerAll)
	bithumbChan.OrderChan = make(chan bithumb.OrderAll)
}
